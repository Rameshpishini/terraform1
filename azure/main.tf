resource "azurerm_resource_group" "myterraformgroup" {
  name     = "myResourceGroup"
  location = "${var.web_server_location}"

  tags = {
    environment = "Terraform Demo"
  }
}

resource "azurerm_virtual_network" "web_server_vnet" {
  address_space       = ["${var.web_server_address_space}"]
  location            = "${var.web_server_location}"
  name                = "${var.resource_prefix}-vnet"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
}

resource "azurerm_subnet" "web_server_subnet" {
  address_prefix       = "10.0.1.0/24"
  name                 = "${var.resource_prefix}-subnet"
  resource_group_name  = "${azurerm_resource_group.myterraformgroup.name}"
  virtual_network_name = "${azurerm_virtual_network.web_server_vnet.name}"
}

resource "azurerm_network_interface" "web_server_nic" {
  location            = "${var.web_server_location}"
  name                = "${var.web_server_name}-nic"
  resource_group_name = "${azurerm_resource_group.myterraformgroup.name}"
  ip_configuration {
    name                          = "${var.web_server_name}-IP"
    subnet_id                     = "${azurerm_subnet.web_server_subnet.id}"
    private_ip_address_allocation = "dynamic"
  }
}