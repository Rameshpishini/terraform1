#!/bin/bash
export ENVIRONMENT=$1
export VARIABLES_FILE=${CI_PROJECT_DIR}/aws_terraform/VPC/backend.tfvars
export PATH=$PATH:~
export TF_STATE_KEY=${CI_COMMIT_REF_SLUG}/vpc.tfstate

echo "Deploying to $ENVIRONMENT"

cd ${CI_PROJECT_DIR}/aws_terraform/VPC/

terraform -version
terraform init -backend=true -backend-config=$VARIABLES_FILE -backend-config="key=${TF_STATE_KEY}"
#terraform plan -refresh=true -var-file=${CI_PROJECT_DIR}/terraform/ECF/ecfwebapp/ecfwebapp.tfvars
terraform destroy -var-file=${CI_PROJECT_DIR}/aws_terraform/VPC/backend.tfvars -auto-approve

