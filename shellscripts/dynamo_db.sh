#!/bin/sh

cd ${CI_PROJECT_DIR}/aws_terraform/aws_dynamo_db/

terraform --version
terraform init ${CI_PROJECT_DIR}/aws_terraform/aws_dynamo_db/
terraform plan