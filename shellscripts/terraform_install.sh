#!/bin/sh
echo "hello world"
wget https://releases.hashicorp.com/terraform/0.12.9/terraform_0.12.9_linux_amd64.zip
unzip terraform_0.12.9_linux_amd64.zip
mv terraform /usr/local/bin/
terraform --version

wget https://releases.hashicorp.com/packer/1.4.4/packer_1.4.4_linux_amd64.zip
unzip packer_1.4.4_linux_amd64.zip
