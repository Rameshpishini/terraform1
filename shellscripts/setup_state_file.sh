#/bin/sh
export ENVIRONMENT=$1
echo "Checking if the Terraform S3 state bucket is available in $1. This might take up to 100 seconds"

aws s3api wait bucket-exists --bucket demo-tf-state-$ENVIRONMENT --region us-east-2 || exitCode=$?

if [ "$exitCode" = "255" ]; then
  echo "The Terraform S3 state bucket is unavailable. We will create it"
  cd ${CI_PROJECT_DIR}/aws_terraform/setup/storage/

  terraform init
  pwd
  terraform plan
  terraform apply -auto-approve
fi
