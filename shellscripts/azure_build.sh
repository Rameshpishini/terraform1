#!/bin/sh

cd ${CI_PROJECT_DIR}/azure/

terraform --version
terraform init ${CI_PROJECT_DIR}/azure/
terraform plan