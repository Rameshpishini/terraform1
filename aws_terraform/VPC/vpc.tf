data "aws_availability_zones" "available" {}

# Main vpc

resource "aws_vpc" "Main" {
  cidr_block = "${var.VPC_CIDR_BLOCK}"
  enable_dns_support = "true"
  enable_dns_hostnames = "true"
  tags = {
    Name = "${var.PROJECT_NAME}-vpc"
  }
}

# Public Subenets

# Public subnet1

resource "aws_subnet" "public_subnet_1" {
  cidr_block = "${var.VPC_PUBLIC_SUBNET1_CIDR_BLOCK}"
  vpc_id = "${aws_vpc.Main.id}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"
  map_public_ip_on_launch = "true"
  tags = {
    Name = "${var.PROJECT_NAME}-vpc-public-subnet-1"
  }
}

# Public subnet2
resource "aws_subnet" "public_subnet_2" {
  cidr_block = "${var.VPC_PUBLIC_SUBNET2_CIDR_BLOCK}"
  vpc_id = "${aws_vpc.Main.id}"
  availability_zone = "${data.aws_availability_zones.available.names[1]}"
  map_public_ip_on_launch = "true"
  tags = {
    Name = "${var.PROJECT_NAME}-vpc-public-subnet-2"
  }
}

# Private Subnet1

resource "aws_subnet" "Private_subnet_1" {
  cidr_block = "${var.VPC_PRIVATE_SUBNET1_CIDR_BLOCK}"
  vpc_id = "${aws_vpc.Main.id}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"
  tags = {
    Name = "${var.PROJECT_NAME}-vpc-private-subnet-1"
  }
}

# Private subnet2

resource "aws_subnet" "private_subnet_2" {
  cidr_block = "${var.VPC_PRIVATE_SUBNET2_CIDR_BLOCK}"
  vpc_id = "${aws_vpc.Main.id}"
  availability_zone = "${data.aws_availability_zones.available.names[1]}"
  tags = {
    Name = "${var.PROJECT_NAME}-vpc-private-subnet-2"
  }
}

# internet gateway

resource "aws_internet_gateway" "IGW" {
  vpc_id = "${aws_vpc.Main.id}"
  tags = {
    Name = "${var.PROJECT_NAME}-IGW"
  }
}

# Elastic IP for NAT Gateway

#resource "aws_eip" "Nat_IP" {
#  vpc = true
#  depends_on = ["aws_internet_gateway.IGW"]
#}

# Nat gateway for private IP

#resource "aws_nat_gateway" "NGW" {
#  allocation_id = "${aws_eip.Nat_IP.id}"
#  subnet_id = "${aws_subnet.Private_subnet_1.id}"
#  depends_on = ["aws_internet_gateway.IGW"]
#  tags = {
#    Name = "${var.PROJECT_NAME}-VPC-Nat-Gateway"
#  }
#}

#Route Table for Public subnet

resource "aws_route_table" "Public" {
  vpc_id = "${aws_vpc.Main.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.IGW.id}"
  }
  tags = {
    Name = "${var.PROJECT_NAME}-Public-route-table"
  }
}

resource "aws_route_table" "Private" {
  vpc_id = "${aws_vpc.Main.id}"
#  route {
#    cidr_block = "0.0.0.0/0"
#    gateway_id = "${aws_nat_gateway.NGW.id}"
#  }
  tags = {
    Name = "${var.PROJECT_NAME}-Private-route-table"
  }
}

# Route tables associte with Public subnet

resource "aws_route_table_association" "Public_subnet_1" {
  route_table_id = "${aws_route_table.Public.id}"
  subnet_id = "${aws_subnet.public_subnet_1.id}"
}

resource "aws_route_table_association" "public_subnet_1" {
  route_table_id = "${aws_route_table.Public.id}"
  subnet_id = "${aws_subnet.public_subnet_2.id}"
}
resource "aws_route_table_association" "private_subnet_1" {
  route_table_id = "${aws_route_table.Private.id}"
  subnet_id = "${aws_subnet.Private_subnet_1.id}"
}

resource "aws_route_table_association" "private_subnet_2" {
  route_table_id = "${aws_route_table.Private.id}"
  subnet_id = "${aws_subnet.private_subnet_2.id}"
}