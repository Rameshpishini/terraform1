resource "aws_db_instance" "RDS_mysql" {
  identifier = "${var.PROJECT_NAME}-rds-mysql"
  allocated_storage = "${var.RDS_ALLOCATED_STORAGE}"
  storage_type = "gp2"
  engine = "${var.RDS_ENGINE}"
  engine_version = "${var.ENGINE_VERSION}"
  instance_class = "${var.DB_INSTANCE_CLASS}"
  backup_retention_period = "${var.BACKUP_RETENTION_PERIOD}"
  publicly_accessible = "${var.PUBLICLY_ACCESSIBLE}"
  username = "${var.RDS_USERNAME}"
  password = "${var.RDS_PASSWORD}"
  vpc_security_group_ids = ["${aws_security_group.rds_sg.id}"]
  db_subnet_group_name = "${aws_db_subnet_group.rds_subnet.name}"
  multi_az = "true"
}

resource "aws_db_subnet_group" "rds_subnet" {
  name = "${var.PROJECT_NAME}-rds_subnet_group"
  description = "db subnet"
  subnet_ids = ["subnet-0e082f81b59160370", "subnet-020308e1898438948"]

  tags = {
    Name = "${var.PROJECT_NAME}-rds-subnet-group"
  }
}

output "rds_prod_endpoint" {
  value = "${aws_db_instance.RDS_mysql.endpoint}"
}