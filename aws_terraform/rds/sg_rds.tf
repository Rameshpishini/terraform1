resource "aws_security_group" "rds_sg" {
  tags = {
    Name = "${var.PROJECT_NAME}-rds-sg"
  }
  name = "${var.PROJECT_NAME}-rds-sg"
  vpc_id = "vpc-09becb3252c9d56f9"
  ingress {
    from_port = 3306
    protocol = "tcp"
    to_port = 3306
    cidr_blocks = ["${var.RDS_CIDR}"]
  }

  egress {
    from_port = 0
    protocol = "-1"
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

}