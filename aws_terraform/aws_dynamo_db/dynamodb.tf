resource "aws_dynamodb_table" "demo_dynamo_db_table" {
  read_capacity = 20
  write_capacity = 20
  hash_key = "UserId"
  range_key = "gametitle"
  name = "gamescores"
  attribute {
    name = "UserId"
    type = "S"
  }
  attribute {
    name = "GameTitle"
    type = "S"
  }
  attribute {
    name = "TopScore"
    type = "N"
  }
  ttl {
    attribute_name = "TimeToExist"
    enabled = false
  }
  global_secondary_index {
    hash_key = "GameTitle"
    name = "GameTitleIndex"
    projection_type = "INCLUDE"
    range_key = "TopScore"
    write_capacity = 10
    read_capacity = 10
    non_key_attributes = ["UserId"]
  }
  tags = {
    Name = "dynamo_db_table_1"
    Evnovironment = "production"
  }
}