resource "aws_s3_bucket" "storage" {
  bucket = "demo-tf-state-${var.env}"

  versioning {
    enabled = true
  }

  tags = {
    Name      = "demo-tf-state-${var.env}"
    env       = "${var.env}"
    terraform = "true"
  }
}

resource "aws_s3_bucket_policy" "policy" {
  bucket = "${aws_s3_bucket.storage.bucket}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowIAMAccess",
            "Effect": "Allow",
            "Principal": {
                "AWS": ${jsonencode(var.role-arns)}
            },
            "Action": [
                "s3:List*",
                "s3:Get*",
                "s3:Put*"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.storage.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.storage.id}"
            ]
        }
        ]
}
EOF
}
