variable env {
  description = "unique environment identifier"
}

variable region {
  default = "us-east-2"
}


variable role-arns {
  description = "list of role arns that will have access to s3 and kms"
  type        = "list"
}
